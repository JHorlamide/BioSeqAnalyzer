version: '3.8'

services:
  redis-cache:
    container_name: redis-server
    image: redis:alpine
    restart: always
    networks:
      - bioseq-networks
    ports:
      - "6379:6379"
    command: redis-server --save 20 1 --loglevel warning
    volumes:
      - redis-cache:/data

  mongodb-server:
    image: mongo:4.4
    container_name: mongodb-server
    networks:
      - bioseq-networks
    ports:
      - "27017:27017"
    env_file:
      - ./env/mongo.env

  mysql:
    container_name: mysql
    image: mysql
    command: --default-authentication-plugin=mysql_native_password
    restart: always
    networks:
      - bioseq-networks
    env_file:
      - ./env/mysql.env
 
  mysql2:
    container_name: mysql2
    image: mysql
    command: --default-authentication-plugin=mysql_native_password
    restart: always
    networks:
      - bioseq-networks
    env_file:
      - ./env/mysql2.env

  user-service:
    container_name: user-service
    build:
      context: ../user-service
      target: production
    # tty: true
    restart: unless-stopped
    ports: [ "7070:7070" ]
    networks:
      - bioseq-networks
    volumes:
      - ../user-service/app:/user-service/app:cached
      - ../user-service/views:/user-service/views:cached
      - ../user-service/prisma:/user-service/prisma:cached
      - migrations:/user-service/prisma/migrations
      - /user-service/node_modules
    env_file: [ ../user-service/.env ]
    depends_on: [ "mysql" ]

  protein-analyzer:
    container_name: protein-analyzer
    build:
      context: ../protein-analyzer
      target: production
    # tty: true
    restart: unless-stopped
    ports: [ "7071:7071" ]
    networks:
      - bioseq-networks
    volumes:
      - ../protein-analyzer/app:/protein-analyzer/app:cached
      - /protein-analyzer/node_modules
    env_file: [ ../protein-analyzer/.env ]
    depends_on: [ "mongodb-server", "redis-cache" ]

  dna-sequence:
    container_name: dna-sequence
    build:
      context: ../dna-sequence
      dockerfile: ../dna-sequence/Dockerfile
    volumes:
      - ../dna-sequence:/app:cached
      - dna-seq-migrations:/app/dna_sequence/migrations
    networks:
      - bioseq-networks
    restart: unless-stopped
    ports: [ "7072:7072" ]
    env_file: [ ../dna-sequence/.env ]
    depends_on: [ "mysql2" ]

  api-gateway:
    container_name: api-gateway
    build:
      context: ../api-gateway
      target: production
    # tty: true
    restart: unless-stopped
    ports: [ "7073:7073" ]
    networks:
      - bioseq-networks
    volumes:
      - ../api-gateway/app:/api-gateway/app:cached
      - /api-gateway/node_modules
    env_file: [ ../api-gateway/.env ]
    depends_on:
      - protein-analyzer
      - dna-sequence
      - user-service

  client:
    container_name: client
    build: ../client
    # tty: true
    restart: unless-stopped
    ports: [ "5173:5173" ]
    networks:
      - bioseq-networks
    volumes:
      - ../client/public:/client/public:cached
      - ../client/src:/client/src:cached
    env_file: [ ../client/.env ]
    depends_on: [ "protein-analyzer", "dna-sequence", "user-service" ]

networks:
  bioseq-networks:

volumes:
  redis-cache:
    driver: local
  migrations:
    driver: local
  dna-seq-migrations:
    driver: local